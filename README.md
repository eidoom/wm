# [wm](https://gitlab.com/eidoom/wm)

* Data 
    * Source: `./index.yaml`
    * Format: [`yaml`](https://yaml.org/)
    * Tool: [`yq`](https://mikefarah.gitbook.io/yq/)
        * Install: `sudo snap install yq`
        * [Convert](https://mikefarah.gitbook.io/yq/v/v4.x/usage/convert#json-to-yaml) `json` to `yaml`: `yq eval -P file.json`
        * [Convert](https://mikefarah.gitbook.io/yq/v/v4.x/usage/convert#yaml-to-json) `yaml` to `json`: `yq eval -j file.yaml`
