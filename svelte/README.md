# Table
Used [a template](https://github.com/sveltejs/template).

## Get started
Install the dependencies
```bash
npm i
```
then start [Rollup](https://rollupjs.org)
```bash
npm run dev
```
and navigate to [localhost:5000](http://localhost:5000).

## Building and running in production mode
To create an optimised version of the app
```bash
npm run build
```
