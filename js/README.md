## Build dependencies
```shell
sudo npm install -g csv2json
```
## Build
```shell
csv2json index.csv index.json
```
