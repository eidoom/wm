// https://htmldom.dev/show-or-hide-table-columns/

const table = document.getElementsByTagName('table')[0];
const headers = Array.from(table.querySelectorAll('th'));
const menu = document.getElementById('menu');

headers.forEach((th, index) => {
    const li = document.createElement('li');
    const label = document.createElement('label');
    const checkbox = document.createElement('input');
    checkbox.setAttribute('type', 'checkbox');
    checkbox.checked = true;

    const text = document.createTextNode(th.textContent);

    label.appendChild(checkbox);
    label.appendChild(text);

    li.appendChild(label);
    menu.appendChild(li);

    checkbox.addEventListener('change', e => {
        e.target.checked ? showColumn(index) : hideColumn(index);
    });
});

const numColumns = headers.length;
const cells = Array.from(table.querySelectorAll('th, td'));
cells.forEach((cell, index) => {
    cell.setAttribute('data-column-index', index % numColumns);
});

const hideColumn = index => {
    cells
        .filter(cell => {
            return cell.getAttribute('data-column-index') === `${index}`;
        })
        .forEach(cell => {
            cell.style.display = 'none';
            // cell.style.visibility = 'collapse';
        });
};

const showColumn = index => {
    cells
        .filter(cell => {
            return cell.getAttribute('data-column-index') === `${index}`;
        })
        .forEach(cell => {
            cell.style.display = '';
            // cell.style.visibility = 'visible';
        });
};
