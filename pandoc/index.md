<ul id="menu"></ul>

| Programme   | Temperature (°C) | Maximum load (kg) | Rated duration (minutes) | Measured duration (minutes) | Water consumption (l) | Electricity consumption (kWh) | Maximum Speed | Minimum Temperature (°C) | Maximum Temperature (°C) |
| ----------- | ---------------- | ----------------- | ------------------------ | --------------------------- | --------------------- | ----------------------------- | ------------- | ------------------------ | ------------------------ |
| Cottons     | 90               | 5                 | 147                      |                             | 66                    | 2.25                          | 1200          | Cold                     | 90                       |
| Cottons     | 60               | 5                 | 118                      |                             | 66                    | 1.45                          | 1200          | Cold                     | 90                       |
| Cottons     | 40               | 5                 | 98                       |                             | 66                    | 0.85                          | 1200          | Cold                     | 90                       |
| Cottons Eco | 60               | 5                 | 190                      |                             | 39                    | 0.74                          | 1200          | 40                       | 60                       |
| Cottons Eco | 60               | 2.5               | 160                      |                             | 32                    | 0.69                          | 1200          | 40                       | 60                       |
| Cottons Eco | 40               | 2.5               | 150                      |                             | 32                    | 0.61                          | 1200          | 40                       | 60                       |
| Synthetics  | 60               | 2.5               | 116                      |                             | 45                    | 0.9                           | 800           | Cold                     | 60                       |
| Synthetics  | 40               | 2.5               | 106                      |                             | 45                    | 0.42                          | 800           | Cold                     | 60                       |
| Woollens    | 40               | 1.5               | 60                       |                             | 40                    | 0.3                           | 600           | Cold                     | 40                       |
| Delicates   | 30               | 2                 | 59                       |                             | 43                    | 0.23                          | 600           | Cold                     | 40                       |

![](img/Waschen_30.svg)![](img/Waschen_30s.svg)![](img/Waschen_30ss.svg)
