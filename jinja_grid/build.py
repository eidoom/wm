#!/usr/bin/env python3

import csv
import jinja2


def main():
    with open("index.csv", "r") as f:
        # reader = csv.reader(f, quoting=csv.QUOTE_NONNUMERIC)
        reader = csv.reader(f)
        # headers = next(reader)
        rows = list(reader)

    file_loader = jinja2.FileSystemLoader("templates")
    env = jinja2.Environment(loader=file_loader, trim_blocks=True, lstrip_blocks=True)
    template = env.get_template("index.jinja")

    output = template.render(title="Washing Machine", rows=rows)
    print(output)
    with open("index.html", "w") as f:
        f.write(output)


if __name__ == "__main__":
    main()
